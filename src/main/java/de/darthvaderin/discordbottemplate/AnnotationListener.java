package de.darthvaderin.discordbottemplate;

import java.awt.Color;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.*;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 *
 * @author DarthVaderin
 * 
 * This file contains Code by MinorTom, wich is Licensed under the MIT License.
 */
public class AnnotationListener {
     private String botprefix = "!";
     
     @EventSubscriber
     public void kannstdunennenwieduwillst(MessageReceivedEvent event) throws MissingPermissionsException, RateLimitException, DiscordException{
        IMessage message = event.getMessage();
        if (message.getContent().startsWith(botprefix)) {
            String command = message.getContent().replaceFirst(botprefix, ""); // Remove prefix
            String[] args = command.split(" ");
/*Mit Prefix*/            
            if(args[0].equalsIgnoreCase("help")){
                message.reply("Hier kommt Hilfe");
                //Weitere Möglichkeit, was man machen könnte:
                // Code by Minortom [Licensed under MIT]
                try{
                    IRole r = message.getGuild().getRolesbyName("testrolle");
                }catch(Exception e){
                    IRole r = message.getGuild().createRole(); 
                    r.changeName("testrolle"); 
                    r.changeColor(Color.yellow);
                }
                // Ende
                message.getAuthor().addRole(r);
            }
/*Ohne Prefix*/
        }else{
            // Code by Minortom [Licensed under MIT]
            if(message.getContent().equalsIgnoreCase("Ich brauche Hilfe")  // Da kennt jemand den Prefix nicht...
                message.reply("hier ist Hilfe!");  // Also senden wir ihm Hilfe!
            // Ende
        }
     }
     
/*weiteres mögliches Event*/
     @EventSubscriber
     public void andereMethode(UserJoinEvent event) throws RateLimitException, DiscordException, MissingPermissionsException{
         event.getUser().getOrCreatePMChannel().sendMessage("Mal ganz privat: Herzlich Willkommen auf diesem neuen Server.");
     }
     
}
